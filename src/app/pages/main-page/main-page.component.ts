import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  public profiles: any = [
    {
      name: 'Konstantina',
      age: 28,
      picture: '../../../assets/img/content/profile/profile-1.jpg'

    },
    {
      name: 'Alina',
      age: 22,
      picture: '../../../assets/img/content/profile/profile-2.jpg'
    },
    {
      name: 'Katrin',
      age: 20,
      picture: '../../../assets/img/content/profile/profile-3.jpg'
    },
    {
      name: 'Kristina',
      age: 30,
      picture: '../../../assets/img/content/profile/profile-4.jpg'
    },
    {
      name: 'Alexandra',
      age: 30,
      picture: '../../../assets/img/content/profile/profile-5.jpg'
    },
    {
      name: 'Elizaveta',
      age: 24,
      picture: '../../../assets/img/content/profile/profile-6.jpg'
    },
    {
      name: 'Sonya',
      age: 25,
      picture: '../../../assets/img/content/profile/profile-7.jpg'
    },
    {
      name: 'Anastasia',
      age: 24,
      picture: '../../../assets/img/content/profile/profile-8.jpg'
    },

  ];

  constructor() { }

  ngOnInit() {
  }

}
