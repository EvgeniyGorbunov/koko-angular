import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page.component';
import { RouterModule } from '@angular/router';
import { ProfilePhotoComponent } from 'src/app/components/profile-photo/profile-photo.component';
import { RegisterFormComponent } from 'src/app/components/register-form/register-form.component';
import { FooterComponent } from 'src/app/components/footer/footer.component';
import { AboutComponent } from 'src/app/components/about/about.component';
import { LandingBarComponent } from 'src/app/components/landing-bar/landing-bar.component';
import { LandingBlogComponent } from 'src/app/components/landing-blog/landing.blog.component';

@NgModule({
  declarations: [
    MainPageComponent,
    ProfilePhotoComponent,
    RegisterFormComponent,
    FooterComponent,
    AboutComponent,
    LandingBarComponent,
    LandingBlogComponent

  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MainPageComponent
      }

    ])

  ],
  exports: [],
  providers: [],
})
export class MainPageModule { }
